%define soversion 3
Name:        openssl
Epoch:       1
Version:     3.0.8
Release:     1
Summary:     Cryptography and SSL/TLS Toolkit
License:     OpenSSL and SSLeay
URL:         https://www.openssl.org/
Source0:     https://www.openssl.org/source/%{name}-%{version}.tar.gz
Source1:     Makefile.certificate

Patch1:      openssl-3.0-build.patch

BuildRequires: gcc gcc-c++ perl make lksctp-tools-devel coreutils util-linux zlib-devel
Requires:    coreutils %{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}

%description
OpenSSL is a robust, commercial-grade, and full-featured toolkit for the
Transport Layer Security (TLS) and Secure Sockets Layer (SSL) protocols.

%package libs
Summary:      A general purpose cryptography library with TLS implementation
Group:        System Environment/Libraries
Requires:     ca-certificates >= 2008-5
Requires:     crypto-policies >= 20180730
Recommends:   openssl-pkcs11%{?_isa}

%description libs
The openssl-libs package contains the libraries that are used
by various applications which support cryptographic algorithms
and protocols.

%package perl
Summary: Perl scripts provided with OpenSSL
Requires: perl-interpreter
Requires: %{name}%{?_isa} = %{epoch}:%{version}-%{release}

%description perl
OpenSSL is a toolkit for supporting cryptography. The openssl-perl
package provides Perl scripts for converting certificates and keys
from other formats to the formats used by the OpenSSL toolkit.

%package devel
Summary:   Development files for openssl
Requires:  %{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}
Requires: pkgconfig

%description devel
%{summary}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info

%description help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build

sslarch=%{_os}-%{_target_cpu}
%ifarch i686
sslarch=linux-elf
%endif

%ifarch x86_64 aarch64
sslflags=enable-ec_nistp_64_gcc_128
%endif

RPM_OPT_FLAGS="$RPM_OPT_FLAGS -Wa,--noexecstack -Wa,--generate-missing-build-notes=yes -DPURIFY $RPM_LD_FLAGS"
./Configure \
	--prefix=%{_prefix} --openssldir=%{_sysconfdir}/pki/tls ${sslflags} \
	zlib enable-camellia enable-seed enable-rfc3779 \
	enable-cms enable-md2 enable-rc5 ${ktlsopt} enable-fips\
	no-mdc2 no-ec2m enable-sm2 enable-sm4 enable-buildtest-c++\
	shared  ${sslarch} $RPM_OPT_FLAGS '-DDEVRANDOM="\"/dev/urandom\""' \
	-Wl,--allow-multiple-definition


%make_build all

%install
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT
# Install OpenSSL.
install -d $RPM_BUILD_ROOT{%{_bindir},%{_includedir},%{_libdir},%{_mandir},%{_libdir}/openssl,%{_pkgdocdir}}

%make_install

# rename so name with actual version
rename so.%{soversion} so.%{version} $RPM_BUILD_ROOT%{_libdir}/*.so.%{soversion}
# create symbolic link
for lib in $RPM_BUILD_ROOT%{_libdir}/*.so.%{version} ; do
     ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`
     ln -s -f `basename ${lib}` $RPM_BUILD_ROOT%{_libdir}/`basename ${lib} .%{version}`.%{soversion}
done

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/certs
install -m644 %{SOURCE1} $RPM_BUILD_ROOT%{_pkgdocdir}/Makefile.certificate

mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc/*.pl $RPM_BUILD_ROOT%{_bindir}
mv $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/misc/tsget $RPM_BUILD_ROOT%{_bindir}


mkdir -p -m755 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/{certs,crl,newcerts,private}
chmod 700 $RPM_BUILD_ROOT%{_sysconfdir}/pki/CA/private

touch -r %{SOURCE1} $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/{openssl.cnf,ct_log_list.cnf}


# rename man pages avoid conflicting with other man pages in system
%define manpostfix _openssl
pushd $RPM_BUILD_ROOT%{_mandir}
ln -s -f config.5 man5/openssl.cnf.5
for manpage in man*/* ; do
    if [ -L ${manpage} ]; then
        targetfile=`ls -l ${manpage} | awk '{print $NF}'`
        ln -sf ${targetfile}%{manpostfix} ${manpage}%{manpostfix}
        rm -f ${manpage}
    else
        mv ${manpage} ${manpage}%{manpostfix}
    fi
done
popd

# Next step of gradual disablement of ssl3.
# Make SSL3 disappear to newly built dependencies.
sed -i '/^\#ifndef OPENSSL_NO_SSL_TRACE/i\
#ifndef OPENSSL_NO_SSL3\
# define OPENSSL_NO_SSL3\
#endif' $RPM_BUILD_ROOT/%{_prefix}/include/openssl/opensslconf.h

basearch=%{_arch}
%ifarch %{ix86}
basearch=i386
%endif

rm -f $RPM_BUILD_ROOT%{_sysconfdir}/pki/tls/*.dist

%check
LD_LIBRARY_PATH=`pwd`${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
export LD_LIBRARY_PATH
OPENSSL_ENABLE_MD5_VERIFY=
export OPENSSL_ENABLE_MD5_VERIFY
OPENSSL_SYSTEM_CIPHERS_OVERRIDE=xyz_nonexistent_file
export OPENSSL_SYSTEM_CIPHERS_OVERRIDE
make test || :

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files
%license LICENSE.txt
%doc NEWS.md README.md
%{_bindir}/openssl
%{_pkgdocdir}/Makefile.certificate

%files libs
%license LICENSE.txt
%dir %{_sysconfdir}/pki/tls
%dir %{_sysconfdir}/pki/tls/certs
%dir %{_sysconfdir}/pki/tls/misc
%dir %{_sysconfdir}/pki/tls/private
%config(noreplace) %{_sysconfdir}/pki/tls/openssl.cnf
%config(noreplace) %{_sysconfdir}/pki/tls/ct_log_list.cnf
%config(noreplace) %{_sysconfdir}/pki/tls/fipsmodule.cnf
%attr(0755,root,root) %{_libdir}/libcrypto.so.%{version}
%{_libdir}/libcrypto.so.%{soversion}
%attr(0755,root,root) %{_libdir}/libssl.so.%{version}
%{_libdir}/libssl.so.%{soversion}
%attr(0755,root,root) %{_libdir}/engines-%{soversion}
%attr(0755,root,root) %{_libdir}/ossl-modules

%files devel
%doc CHANGES.md doc/dir-locals.example.el doc/openssl-c-indent.el
%{_prefix}/include/openssl
%{_libdir}/*.so
%{_libdir}/*.a
%{_mandir}/man3/*
%{_libdir}/pkgconfig/*.pc


%files help
%defattr(-,root,root)
%{_mandir}/man1/*
%{_mandir}/man5/*
%{_mandir}/man7/*
%exclude %{_mandir}/man1/*.pl*
%exclude %{_mandir}/man1/tsget*

%files perl
%{_bindir}/c_rehash
%{_bindir}/*.pl
%{_bindir}/tsget
%{_mandir}/man1/*.pl*
%{_mandir}/man1/tsget*
%dir %{_sysconfdir}/pki/CA
%dir %{_sysconfdir}/pki/CA/private
%dir %{_sysconfdir}/pki/CA/certs
%dir %{_sysconfdir}/pki/CA/crl
%dir %{_sysconfdir}/pki/CA/newcerts

%ldconfig_scriptlets libs

%changelog
* Tue Feb 7 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.8-1
- upgrade to 3.0.8 for fixing CVEs

* Tue Feb 7 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.7-2
- disable sctp in openssl building

* Thu Jan 19 2023 wangcheng <wangcheng156@huawei.com> - 1:3.0.7-1
- Package init


